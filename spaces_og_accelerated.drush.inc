<?php

/**
 * @file
 * Drush integration for the spaces_og_accelerated module.
 */

/**
 * Implements hook_drush_command().
 */
function spaces_og_accelerated_drush_command() {
  $items['cloaking-device-download'] = array(
    'description' => dt('Downloads the cloaking-device library from https://github.com/pdrakeweb/cloaking-device.git.'),
    'arguments' => array(
      'path' => dt('Optional. A path to the download folder. If omitted Drush will use the sites/all/libraries/cloaking-device.'),
    ),
    'aliases' => array('cloakd'),
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function spaces_og_accelerated_drush_help($section) {
  switch ($section) {
    case 'drush:cloaking-device-download':
      return dt('Downloads the cloaking-device library from https://github.com/pdrakeweb/cloaking-device.git. Places it in the libraries directory. Skips download if library already present. This all happens automatically if you enable spaces_og_accelerated using drush.');
  }
}

/**
 * A command callback. Download the cloaking-device library using git.
 */
function drush_cloaking_device_download() {
  $args = func_get_args();
  if (isset($args[0])) {
    $path = $args[0];
  }
  else {
    $path = drush_get_context('DRUSH_DRUPAL_ROOT');
    if (function_exists('libraries_get_path')) {
      $path .= '/' . libraries_get_path('cloaking-device');
    }
  }

  if (is_dir($path)) {
    drush_log(' already present. No download required.', 'ok');
  }
  elseif (drush_shell_cd_and_exec(dirname($path), 'git clone https://github.com/pdrakeweb/cloaking-device.git')) {
    drush_log(dt('cloaking-device has been cloned via git to @path.', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to clone cloaking-device to @path.', array('@path' => $path)), 'error');
  }
}

/**
 * Implements drush_MODULE_post_COMMAND().
 */
function drush_spaces_og_accelerated_post_pm_enable() {
  $modules = func_get_args();
  if (in_array('spaces_og_accelerated', $modules)) {
    drush_cloaking_device_download();
  }
}
