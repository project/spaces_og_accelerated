The spaces_og_accelerated module requires the cloaking-device PHP library.

To install, use one of the following methods:

1. Checkout the cloaking-device PHP library from github like so (if using the libraries module):

    # From your site's docroot.
    git clone https://github.com/pdrakeweb/cloaking-device.git sites/all/libraries/cloaking-device

2. Install via Drush (if using the libraries module):

    # From your site's docroot.
    drush cloaking-device-download

3. Install the module via composer, or in your own composer.json:
    {
      "require":{
        "allplayers/cloaking-device": "dev-master"
      }
    }
