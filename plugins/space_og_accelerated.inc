<?php

use CloakingDevice\Drupal\GroupNode as GroupNode;

/**
 * Accelerated organic groups integration for Spaces.
 */
class space_og_accelerated extends space_og {

  /**
   * Override of load().
   */
  function load() {
    // The 0 id means a new group is being saved. Instantiate a space
    // so preset values can become active.
    if ($this->id === 0) {
      return TRUE;
    }
    elseif ($this->group = new GroupNode($this->id)) {
      return TRUE;
    }
    return FALSE;
  }

}
